package com.example.nguyendinhhien_ps10199;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

public class LoginActivity extends AppCompatActivity {

    EditText edtUsername,edtPassword;
    CheckBox chkSaveInfo;
    Button btnLogin;
    SharedPreferences storage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //ánh xạ
        edtUsername = findViewById(R.id.edtUsername);
        edtPassword = findViewById(R.id.edtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        chkSaveInfo=findViewById(R.id.chkSaveInfo);

        //lưu usernamaevà mật khẩu
        storage=getSharedPreferences("myfile", Context.MODE_PRIVATE);

        //nạp thông tin lên form từ sharedPreference
        Boolean saveInfo=storage.getBoolean("save_infomation",false);
        if(saveInfo)
        {
            edtUsername.setText(storage.getString("username",""));
            edtPassword.setText(storage.getString("password",""));
            chkSaveInfo.setChecked(true);
        }

    }

    //hàm xử lý đăng nhập
    public void Login(View v)
    {
        String username=edtUsername.getText().toString();
        String password=edtPassword.getText().toString();
        if(username.equals("admin")&&password.equals("123"))
        {
            Toast.makeText(getApplicationContext(),"Login success!!!",Toast.LENGTH_SHORT).show();
            //kiểm tra checkbox đã check thì lưu dữ liệu đăng nhập
            SharedPreferences.Editor editor=storage.edit();
            if(chkSaveInfo.isChecked())
            {

                editor.putString("username",username);
                editor.putString("password",password);
            }
            editor.putBoolean("save_infomation",true);
            editor.commit();

            Intent intent=new Intent(LoginActivity.this,MainActivity.class);
            startActivity(intent);
        }
        else
        {
            edtUsername.setText("");
            edtPassword.setText("");
        }
    }

}