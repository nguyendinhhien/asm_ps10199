package com.example.nguyendinhhien_ps10199.model;

import java.util.Date;

public class TestSchedule {
    int testScheduleCode;
    String courseCode,room;
    Date testDate;

    public TestSchedule() {
    }

    public TestSchedule(int testScheduleCode,String courseCode, String room, Date testDate) {
        this.testScheduleCode=testScheduleCode;
        this.courseCode = courseCode;
        this.room = room;
        this.testDate = testDate;
    }

    public int getTestScheduleCode() {
        return testScheduleCode;
    }

    public void setTestScheduleCode(int testScheduleCode) {
        this.testScheduleCode = testScheduleCode;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public Date getTestDate() {
        return testDate;
    }

    public void setTestDate(Date testDate) {
        this.testDate = testDate;
    }
}
