package com.example.nguyendinhhien_ps10199.model;

import java.util.Date;

public class CourseSchedule {
    int courseScheduleCode;
    String courseCode,courseContent,room;
    Date studyDate;

    public CourseSchedule() {
    }

    public CourseSchedule(int courseScheduleCode,String courseCode, String courseContent, String room, Date studyDate) {
        this.courseScheduleCode=courseScheduleCode;
        this.courseCode = courseCode;
        this.courseContent = courseContent;
        this.room = room;
        this.studyDate = studyDate;
    }

    public int getCourseScheduleCode() {
        return courseScheduleCode;
    }

    public void setCourseScheduleCode(int courseScheduleCode) {
        this.courseScheduleCode = courseScheduleCode;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getCourseContent() {
        return courseContent;
    }

    public void setCourseContent(String courseContent) {
        this.courseContent = courseContent;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public Date getStudyDate() {
        return studyDate;
    }

    public void setStudyDate(Date studyDate) {
        this.studyDate = studyDate;
    }

}
