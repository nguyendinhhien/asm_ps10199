package com.example.nguyendinhhien_ps10199.model;

import java.util.Date;

public class Course {
    String courseCode,courseName,teacherName;
    int creditNumber;
    Date startDate,endDate;

    public Course() {
    }

    public Course(String courseCode, String courseName, String teacherName,int creditNumber, Date startDate, Date endDate) {
        this.courseCode = courseCode;
        this.courseName = courseName;
        this.teacherName = teacherName;
        this.creditNumber=creditNumber;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public int getCreditNumber() {
        return creditNumber;
    }

    public void setCreditNumber(int creditNumber) {
        this.creditNumber = creditNumber;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
