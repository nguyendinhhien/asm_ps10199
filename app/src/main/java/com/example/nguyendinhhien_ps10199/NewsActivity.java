package com.example.nguyendinhhien_ps10199;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.nguyendinhhien_ps10199.adapter.NewsAdapter;
import com.example.nguyendinhhien_ps10199.model.News;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class NewsActivity extends AppCompatActivity {

    ListView lvNews;
    NewsAdapter newsAdapter;
    News news;
    List<News> newes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        initComponent();
        NewsSeedAsyncTask newsSeedAsyncTask = new NewsSeedAsyncTask();
        newsSeedAsyncTask.execute();
        itemNewsClick();
    }

    public void initComponent(){
        lvNews = findViewById(R.id.lvNews);
    }

    class NewsSeedAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL url = new URL("https://vnexpress.net/rss/giao-duc.rss");
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                newes = NewsReader.listNews(inputStream);
                newsAdapter = new NewsAdapter(NewsActivity.this, newes);
                NewsActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        lvNews.setAdapter(newsAdapter);

                    }
                });
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

    }

    public void itemNewsClick() {
        lvNews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                news = newes.get(position);
                Intent intent = new Intent(NewsActivity.this, NewsWebViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("link", news.getLink());
                intent.putExtra("new", bundle);
                startActivity(intent);
            }
        });
    }
}
