package com.example.nguyendinhhien_ps10199.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nguyendinhhien_ps10199.R;
import com.example.nguyendinhhien_ps10199.dao.CourseDAO;
import com.example.nguyendinhhien_ps10199.model.Course;


import java.text.SimpleDateFormat;
import java.util.List;

public class CourseAdapter extends BaseAdapter {
    List<Course> list;
    public Context context;
    public LayoutInflater inflater;
    CourseDAO courseDAO;
    CourseAdapter.ViewHolder holder;
    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy");

    public CourseAdapter(List<Course> list, Context context) {
        super();
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        courseDAO= new CourseDAO(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Course course = list.get(position);
        if (convertView == null) {
            holder = new CourseAdapter.ViewHolder();
            convertView = inflater.inflate(R.layout.one_cell_course_layout, null);

            //ánh xạ
            holder.ivIcon= (ImageView) convertView.findViewById(R.id.ivIcon);
            holder.tvCourseName= (TextView) convertView.findViewById(R.id.tvCourseName);
            holder.tvCreditNumber = (TextView) convertView.findViewById(R.id.tvCreditNumber);
            holder.tvTeacherName = (TextView) convertView.findViewById(R.id.tvTeacherName);
            holder.tvStartDate = (TextView) convertView.findViewById(R.id.tvStartDate);
            holder.tvEndDate = (TextView) convertView.findViewById(R.id.tvEndDate);
            holder.ivAdd= (ImageView) convertView.findViewById(R.id.ivAdd);

            //set data lên layout custom
            holder.tvCourseName.setText(course.getCourseName());
            holder.tvCreditNumber.setText(course.getCreditNumber()+"");
            holder.tvTeacherName.setText(course.getTeacherName());
            holder.tvStartDate.setText(simpleDateFormat.format(course.getStartDate()));
            holder.tvEndDate.setText(simpleDateFormat.format(course.getEndDate()));

            convertView.setTag(holder);
        } else
            holder = (CourseAdapter.ViewHolder) convertView.getTag();

        //thêm môn học vào môn học của tôi
        holder.ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Thêm thành công",Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }

    public static class ViewHolder {
        ImageView ivIcon;
        TextView tvCourseName;
        TextView tvCreditNumber;
        TextView tvTeacherName;
        TextView tvStartDate;
        TextView tvEndDate;
        ImageView ivAdd;
    }
}
