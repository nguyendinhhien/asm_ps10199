package com.example.nguyendinhhien_ps10199.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.nguyendinhhien_ps10199.R;
import com.example.nguyendinhhien_ps10199.model.News;

import java.util.List;

public class NewsAdapter extends BaseAdapter {

    Context context;
    List<News> list;
    LayoutInflater layoutInflater;
    ViewHolder holder;

    public NewsAdapter(Context context, List<News> list) {
        this.context = context;
        this.list = list;
        layoutInflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    public static class ViewHolder{
        TextView tvTitle;
        TextView tvLink;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        News news=list.get(position);
        if(convertView==null){
            holder=new ViewHolder();
            convertView=layoutInflater.inflate(R.layout.one_cell_news_layout,null);

            holder.tvTitle=(TextView)convertView.findViewById(R.id.tvTitle);
            holder.tvLink=(TextView)convertView.findViewById(R.id.tvLink);

            convertView.setTag(holder);
        }
        else
            holder=(ViewHolder)convertView.getTag();

        holder.tvTitle.setText(news.getTitle());
        holder.tvLink.setText(news.getLink());
        return convertView;
    }
}
