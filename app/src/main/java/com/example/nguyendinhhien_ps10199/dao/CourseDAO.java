package com.example.nguyendinhhien_ps10199.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.nguyendinhhien_ps10199.dbhelper.DBHelper;
import com.example.nguyendinhhien_ps10199.model.Course;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class CourseDAO {
    DBHelper dbHelper;
   ;
    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy");

    public CourseDAO(Context context) {
        dbHelper = new DBHelper(context);
    }

    public ArrayList<Course> getAll() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<Course> list = new ArrayList<>();
        String sql = "SELECT * FROM COURSE";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            Course course=new Course();
            course.setCourseCode(c.getString(0));
            course.setCourseName(c.getString(1));
            course.setTeacherName(c.getString(2));
            try {
                course.setStartDate(simpleDateFormat.parse(c.getString(3)));
                course.setEndDate(simpleDateFormat.parse(c.getString(4)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            list.add(course);
            c.moveToNext();
        }
        return list;
    }


    public long insert(Course course) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put("COURSE_CODE",course.getCourseCode());
        values.put("COURSE_NAME",course.getCourseName());
        values.put("TEACHER_NAME",course.getTeacherName());
        values.put("START_DATE",simpleDateFormat.format(course.getStartDate()));
        values.put("END_DATE",simpleDateFormat.format(course.getEndDate()));
        return db.insert("COURSE",null,values);
    }

    public int update(Course course) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put("COURSE_CODE",course.getCourseCode());
        values.put("COURSE_NAME",course.getCourseName());
        values.put("TEACHER_NAME",course.getTeacherName());
        values.put("START_DATE",course.getStartDate().toString());
        values.put("END_DATE",course.getEndDate().toString());
        return db.update("COURSE",values,"COURSE_CODE=?",new String[]{course.getCourseCode()});
    }

    public void delete() {

    }
}
