package com.example.nguyendinhhien_ps10199;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    ImageView ivCourses,ivMaps,ivNews,ivSocial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponent();

        courses();
        maps();
        news();
        social();
    }

    public void initComponent(){
        ivCourses=findViewById(R.id.ivCourses);
        ivMaps=findViewById(R.id.ivMaps);
        ivNews=findViewById(R.id.ivNews);
        ivSocial=findViewById(R.id.ivSocial);
    }

    public void courses(){
        ivCourses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,CuorseActivity.class);
                startActivity(intent);
            }
        });
    }

    public void maps(){
        ivMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,MapsActivity.class);
                startActivity(intent);
            }
        });
    }

    public void news(){
        ivNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,NewsActivity.class);
                startActivity(intent);
            }
        });
    }

    public void social(){
        ivSocial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,SocialActivity.class);
                startActivity(intent);
            }
        });
    }
}
