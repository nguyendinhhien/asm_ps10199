package com.example.nguyendinhhien_ps10199.dbhelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    public final static String DBNAME = "QUANLYHOCTAP";
    public final static int DBVERSION = 1;

    public DBHelper(Context context) {
        super(context, DBNAME, null, DBVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql;
        //creat course table
        sql = "CREATE TABLE COURSE(" +
                "COURSE_CODE TEXT PRIMARY KEY," +
                "COURSE_NAME TEXT NOT NULL," +
                "TEACHER_NAME TEXT NOT NULL," +
                "START_DATE DATE NOT NULL," +
                "END_DATE DATE NOT NULL)";
        db.execSQL(sql);

        //creat course schedule table
        sql = "CREATE TABLE COURSE_SCHEDULE(" +
                "COURSE_SCHEDULE_CODE INTEGER PRIMARY KEY AUTOINCREMENT," +
                "COURSE_CODE TEXT REFERENCES COURSE(COURSE_CODE)," +
                "COURSE_CONTENT TEXT NOT NULL," +
                "ROOM TEXT NOT NULL," +
                "STUDY_DATE DATE NOT NULL)";
        db.execSQL(sql);

        //creat test schedule table
        sql = "CREATE TABLE TEST_SCHEDULE(" +
                "TEST_SCHEDULE_CODE INTEGER PRIMARY KEY AUTOINCREMENT," +
                "COURSE_CODE TEXT REFERENCES COURSE(COURSE_CODE)," +
                "ROOM TEXT NOT NULL," +
                "TEST_DATE DATE NOT NULL)";
        db.execSQL(sql);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql;
        sql = "DROP TABLE IF EXISTS COURSE";
        db.execSQL(sql);
        sql = "DROP TABLE IF EXISTS COURSE_SCHEDULE";
        db.execSQL(sql);
        sql = "DROP TABLE IF EXISTS TEST_SCHEDULE";
        db.execSQL(sql);
        onCreate(db);
    }
}
