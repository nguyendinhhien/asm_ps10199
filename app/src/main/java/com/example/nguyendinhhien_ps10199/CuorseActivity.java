package com.example.nguyendinhhien_ps10199;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.example.nguyendinhhien_ps10199.adapter.CourseAdapter;
import com.example.nguyendinhhien_ps10199.dao.CourseDAO;
import com.example.nguyendinhhien_ps10199.model.Course;

import java.util.ArrayList;

public class CuorseActivity  extends AppCompatActivity {
    ListView lvCourseList;
    Course course;
    ArrayList<Course> list;
    CourseDAO courseDAO;
    CourseAdapter courseAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuorse);

        initComponent();
        showCourseList();
    }

    public void initComponent(){
        lvCourseList=findViewById(R.id.lvCourseList);
    }

    public void showCourseList(){
        course=new Course();
        list=new ArrayList<>();
        courseDAO=new CourseDAO(CuorseActivity.this);
        list=courseDAO.getAll();
        courseAdapter=new CourseAdapter(list,CuorseActivity.this);
        lvCourseList.setAdapter(courseAdapter);
    }
}
